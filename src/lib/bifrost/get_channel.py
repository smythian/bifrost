#!/usr/bin/env python

import argparse
import json
import re
import subprocess

from syslog import syslog, LOG_ERR, LOG_WARNING, LOG_DEBUG
from time import sleep
from threading import Timer


DEFAULT_CHANNEL = 3


def parse_args():
    parser = argparse.ArgumentParser(description='''
    Simple script to find the "best" WiFi channel to use. Since the RPi driver
    doesn't support ACS, this scan recommends a channel by only considering the
    in use channels (identified by an iw scan) and the signal strength of the
    identified AP on those channels. Algorithm maintains a sum of signal
    quality per channel and accounts for signal bleed by adding a fraction of
    the signal quality to adjacent channels. Upon completion of the scan, the
    channel with the least signal is chosen.
    ''')
    parser.add_argument('-i', '--iface')
    return parser.parse_args()


def __scan__(iface):
    kill = lambda process: process.kill()
    proc = subprocess.Popen(['iw', 'dev', iface, 'scan'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    timer = Timer(15, kill, [proc])
    try:
        timer.start()
        stdout, stderr = proc.communicate()
        return stdout
    finally:
        timer.cancel()


def scan(iface, num_scans=3):
    syslog(LOG_DEBUG, "Starting {0} scans".format(num_scans))
    result = DEFAULT_CHANNEL

    if num_scans > 0:
        wifi_scans = list()
        for _ in range(0, num_scans):
            try:
                wifi_scans.append(__scan__(iface))
            except subprocess.CalledProcessError as ex:
                syslog(LOG_ERR,
                       json.dumps({"msg": ex.message, "return_code": ex.returncode, "cmd": ex.cmd,
                                   "output": ex.output}))
                sleep(1)
        if wifi_scans:
            result = "\n".join(wifi_scans)
    else:
        syslog(LOG_ERR, "Invalid scan options")
    return result


def get_channel(iface):
    syslog(LOG_DEBUG, "Scanning and setting channel")
    scan_results = scan(iface)
    return __parse_iw_scan__(scan_results)


def __get_channels__():
    return list(range(1, 12))


def __parse_iw_scan__(wifi_scan):
    _BSS_RE = r'(?<=^BSS )([a-f0-9]{2}:){5}[a-f0-9]{2}'
    _CH_RE = r'(?<=DS Parameter set: channel )\d\d?'
    _SIG_RE = r'(?<=signal: )-\d{1,3}\.\d{2}'
    in_use = dict()
    channel = None
    quality = None
    station = None
    result = None

    for line in wifi_scan.splitlines():
        if line.startswith("BSS"):
            if channel and quality:
                syslog(LOG_DEBUG, "station: {0} channel: {1} quality: {2}".format(station, channel, quality))
                __add_to_in_use__(in_use, channel, quality)
                for i in range(1, 3):
                    degraded_quality = __calculate_degraded_signal__(quality, i)
                    __add_to_in_use__(in_use, channel + i, degraded_quality)
                    __add_to_in_use__(in_use, channel - i, degraded_quality)

            channel = None
            quality = None
            station = re.search(_BSS_RE, line).group(0)
        elif re.search(_SIG_RE, line):
            quality = __dbm_to_quality__(float(re.search(_SIG_RE, line).group(0)))
        elif re.search(_CH_RE, line):
            channel = int(re.search(_CH_RE, line).group(0))

    if in_use:
        result = min(in_use, key=in_use.get)
    else:
        syslog(LOG_WARNING, "Scan did not identify any in-use channels")

    if not isinstance(result, int) or channel not in __get_channels__():
        syslog(LOG_ERR, "Simple channel survey identified an invalid channel: {0}".format(channel))
        result = None

    return result


def __add_to_in_use__(in_use, channel, quality):
    if channel in __get_channels__():
        if channel not in in_use:
            in_use[channel] = 0
        in_use[channel] += quality


def __calculate_degraded_signal__(quality, channels_away):
    return quality / float(channels_away + 1)


def __dbm_to_quality__(sig_dbm):
    if sig_dbm <= -100:
        result = 0
    elif sig_dbm >= -50:
        result = 100
    else:
        result = 2 * (sig_dbm + 100)

    return result


if __name__ == '__main__':
    args = parse_args()
    print get_channel(args.iface)
