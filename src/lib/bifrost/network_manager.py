#!/usr/bin/env python

from syslog import syslog, LOG_DEBUG
from dbus_helper import get_interface_helper, set_property_helper, get_property_helper, to_variant, GLib_Error

BUS_NAME = "org.freedesktop.NetworkManager"

NM_OBJ_PATH = "/org/freedesktop/NetworkManager"
NM_IFACE_NAME = "org.freedesktop.NetworkManager"

SETTINGS_OBJ_PATH = "/org/freedesktop/NetworkManager/Settings"
SETTINGS_IFACE_NAME = "org.freedesktop.NetworkManager.Settings"

DEVICE_IFACE_NAME = "org.freedesktop.NetworkManager.Device"

CONN_OBJ_PATH = "/org/freedesktop/NetworkManager/Settings/Connection"
CONN_IFACE_NAME = "org.freedesktop.NetworkManager.Settings.Connection"


def _build_connection_obj(uuid, ssid, psk):
    return {
        'connection': {
            'type': to_variant('802-11-wireless'),
            'uuid': to_variant(uuid),
            'id': to_variant(ssid),
        },
        '802-11-wireless': {
            'ssid': to_variant(bytearray(ssid)),
            'mode': to_variant('infrastructure'),
        },
        '802-11-wireless-security': {
            'key-mgmt': to_variant('wpa-psk'),
            'psk': to_variant(psk),
        },
        'ipv4': {'method': to_variant('auto')},
        'ipv6': {'method': to_variant('auto')},
    }


def add_connection(uuid, ssid, psk):
    syslog(LOG_DEBUG, "Adding a connection")
    dbus_iface = get_interface_helper(BUS_NAME, SETTINGS_OBJ_PATH, SETTINGS_IFACE_NAME)
    return dbus_iface.AddConnection(_build_connection_obj(uuid, ssid, psk))


def update_connection(connection_path, uuid, ssid, psk):
    syslog(LOG_DEBUG, "Updating a connection")
    dbus_iface = get_interface_helper(BUS_NAME, connection_path, CONN_IFACE_NAME)
    return dbus_iface.Update(_build_connection_obj(uuid, ssid, psk))


def activate_connection(UUID, ssid, psk, iface):
    syslog(LOG_DEBUG, "Activating a connection")
    device_path = get_interface_helper(BUS_NAME, NM_OBJ_PATH, NM_IFACE_NAME).GetDeviceByIpIface(iface)
    try:
        connection_path = get_connection_by_uuid(UUID)
        update_connection(connection_path, UUID, ssid, psk)
    except GLib_Error:
        connection_path = add_connection(UUID, ssid, psk)

    return get_interface_helper(BUS_NAME, NM_OBJ_PATH, NM_IFACE_NAME)\
        .ActivateConnection(connection_path, device_path, "/")


def list_connections():
    syslog(LOG_DEBUG, "Listing all connections")
    return get_interface_helper(BUS_NAME, SETTINGS_OBJ_PATH, SETTINGS_IFACE_NAME).ListConnections()


def get_connection_settings(connection_obj):
    syslog(LOG_DEBUG, "Getting connection settings for: {0}".format(connection_obj))
    return get_interface_helper(BUS_NAME, connection_obj, CONN_IFACE_NAME).GetSettings()


def get_connection_by_uuid(uuid):
    syslog(LOG_DEBUG, "Getting connection object for UUID: {0}".format(uuid))
    return get_interface_helper(BUS_NAME, SETTINGS_OBJ_PATH, SETTINGS_IFACE_NAME).GetConnectionByUuid(uuid)


def get_all_devices():
    syslog(LOG_DEBUG, "Listing all devices")
    return get_interface_helper(BUS_NAME, NM_OBJ_PATH, NM_IFACE_NAME).GetAllDevices()


def get_device_by_ip_iface(iface):
    syslog(LOG_DEBUG, "Getting device for iface: {0}".format(iface))
    return get_interface_helper(BUS_NAME, NM_OBJ_PATH, NM_IFACE_NAME).GetDeviceByIpIface(iface)


def get_device_property(iface_name, prop_name):
    syslog(LOG_DEBUG, "Getting prop {0} for iface {1}".format(prop_name, iface_name))
    iface = get_device_by_ip_iface(iface_name)
    return get_property_helper(BUS_NAME, iface, DEVICE_IFACE_NAME, prop_name)


def set_device_property(iface_name, prop_name, prop_value):
    iface = get_device_by_ip_iface(iface_name)
    set_property_helper(BUS_NAME, iface, DEVICE_IFACE_NAME, prop_name, prop_value)
    syslog(LOG_DEBUG, "Set prop '{0}' on iface '{1}' to '{2}'".format(prop_name, iface_name, prop_value))
