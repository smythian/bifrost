#!/bin/bash

ipt=/sbin/iptables
wlan_iface=${1:-wlan0}
repeater_iface=${2:-wlan1}

flush_or_create_chain() {
  local chain_name="$1"
  local table="${2:-filter}"

  $ipt -n -t "${table}" -L"${chain_name}" &> /dev/null
  if [[ $? -eq 0 ]]; then
    echo "Flushing '${chain_name}' chain on table '${table}'"
    $ipt -t "${table}" -F "${chain_name}" # Delete existing chain
  else
    echo "Creating '${chain_name}' chain on table '${table}'"
    $ipt -t "${table}" -N "${chain_name}" # Create new chain
  fi
}

insert_rule_if_needed() {
  local src="${1}"
  local dst="${2}"
  local table="${3:-filter}"

  $ipt -t "${table}" -C "${src}" -j "${dst}" &> /dev/null
  if [[ $? -gt 0 ]]; then
    echo "Inserting rule to '${src}' on table '${table}' to jump to '${dst}''"
    $ipt -t "${table}" -I "${src}" 1 -j "${dst}"
  fi
}

###########################################
# Drop invalid packets chain
###########################################
flush_or_create_chain "drop-invalid"

$ipt -A drop-invalid -m conntrack --ctstate INVALID -j DROP
# Drop all packets with bad TCP flags
$ipt -A drop-invalid -p tcp --tcp-flags FIN,ACK FIN -j DROP
$ipt -A drop-invalid -p tcp --tcp-flags ACK,PSH PSH -j DROP
$ipt -A drop-invalid -p tcp --tcp-flags ACK,URG URG -j DROP
$ipt -A drop-invalid -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
$ipt -A drop-invalid -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
$ipt -A drop-invalid -p tcp --tcp-flags FIN,RST FIN,RST -j DROP

###########################################
# Bifrost OUTPUT chain
###########################################
flush_or_create_chain "bifrost-output"

$ipt -A bifrost-output -j drop-invalid

insert_rule_if_needed "OUTPUT" "bifrost-output"

###########################################
# Bifrost INPUT chain
###########################################
flush_or_create_chain "bifrost-input"

$ipt -A bifrost-input -j drop-invalid
# Accept traffic on loopback but drop any 127 traffic not on loopback
$ipt -A bifrost-input -i !lo -s 127.0.0.0/8 -j DROP

insert_rule_if_needed "INPUT" "bifrost-input"

###########################################
# Bifrost FORWARD chain
###########################################
flush_or_create_chain "bifrost-forward"

$ipt -A bifrost-forward -j drop-invalid
$ipt -A bifrost-forward -i ${wlan_iface} -o ${repeater_iface} -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
$ipt -A bifrost-forward -i ${repeater_iface} -o ${wlan_iface} -j ACCEPT

insert_rule_if_needed "FORWARD" "bifrost-forward"

###########################################
# NAT it up
###########################################
flush_or_create_chain "bifrost-nat" "nat"

$ipt -t nat -A bifrost-nat -o ${wlan_iface} -j MASQUERADE

insert_rule_if_needed "POSTROUTING" "bifrost-nat" "nat"

