#!/bin/bash
. /tmp/bifrost/exit_codes.sh

# If a variable will be used in another file, prefix with BIFROST_CONFIG_ to ensure that the build check will find errors

###########################################
# Internal Constants
###########################################

# Filesystem paths
service_script_dir="/etc/init.d"
bifrost_init_service="${service_script_dir}/bifrost-init"
dnsmasq_service="${service_script_dir}/dnsmasq"
hostapd_service="${service_script_dir}/hostapd"
dnsmasq_conf="/etc/bifrost/dnsmasq.default.conf"

###########################################
# Bifrost Config Constants
###########################################

# Network config
BIFROST_CONFIG_IP_PART="192.168.10"
# The interface that will connect to the existing wireless network
BIFROST_CONFIG_WLAN_IFACE="wlan1"
# The interface that will provide the repeater wireless access point
BIFROST_CONFIG_REPEATER_IFACE="wlan0"
BIFROST_CONFIG_REPEATER_SSID="Bifrost"
BIFROST_CONFIG_REPEATER_IP="${BIFROST_CONFIG_IP_PART}.1"
BIFROST_CONFIG_DHCP_LEASE_TIME="12h"

###########################################
# Functions
###########################################
configure () {
  local filename=${!1} && shift
  local settings=("$@")

  if [ -f ${filename} ]; then
    echo "Configuring ${#settings[@]} values in ${filename}"
    for setting in "${settings[@]}"; do
      # Report error if no occurrences of config value were found in file
      count=$(grep -c "${setting}" "${filename}")
      if [ "${count}" -eq 0 ]; then
        >&2 echo "No instances of '${setting}' in '${filename}'"
      fi

      sed -i "s@${setting}@${!setting}@g" "${filename}"
    done
  else
    >&2 echo "${filename} does not exist"
  fi
}

# Exit as unsuccessful if not all files are configured
check_for_missed_config () {
  local matches=$(find ${1:-/} -xdev -type f -print0 | xargs -0 grep -Hs -m 1 "BIFROST_CONFIG_" --exclude={set-defaults.sh,test_*,*.test.*})
  local count=0

  if [ ! -z "${matches}" ]; then
    count=$(echo "${matches}" | wc -l)
  fi

  if [ "${count}" -gt 0 ]; then
    >&2 echo "Didn't set "${count}" config values:"
    >&2 echo "${matches}"
    exit ${missing_config_value_error}
  fi
}

###########################################
# Configure conf files
###########################################

configure dnsmasq_conf BIFROST_CONFIG_IP_PART BIFROST_CONFIG_DHCP_LEASE_TIME

###########################################
# Configure service scripts
###########################################
configure hostapd_service BIFROST_CONFIG_REPEATER_SSID BIFROST_CONFIG_REPEATER_IFACE

configure dnsmasq_service BIFROST_CONFIG_REPEATER_IFACE

configure bifrost_init_service BIFROST_CONFIG_WLAN_IFACE BIFROST_CONFIG_REPEATER_IFACE BIFROST_CONFIG_REPEATER_IP

###########################################
# Wrap it up. We're done here.
###########################################
check_for_missed_config
