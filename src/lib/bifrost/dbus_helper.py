from pydbus import SystemBus
from time import sleep
from syslog import syslog, LOG_DEBUG, LOG_ERR
from gi.repository.GLib import Variant, Error

t = 30
GLib_Error = Error

while True:
    try:
        sys_bus = SystemBus()
        break
    except Exception:
        pass
    sleep(1)
    if --t < 0:
        syslog(LOG_ERR, "Initialize SystemBus Timeout")
        break


def get_proxy_helper(bus, obj):
    syslog(LOG_DEBUG, "Getting proxy to object: {0} on bus: {1}".format(obj, bus))
    return sys_bus.get(bus, obj)


def get_interface_helper(bus, obj, interface):
    syslog(LOG_DEBUG, "Getting interface {0} from proxy: {1} {2}".format(interface, bus, obj))
    return get_proxy_helper(bus, obj)[interface]


def get_property_helper(bus, obj, interface, prop):
    syslog(LOG_DEBUG, "Getting property {0} on from: {1} {2} {3}".format(prop, bus, obj, interface))
    return get_interface_helper(bus, obj, "org.freedesktop.DBus.Properties").Get(interface, prop)


def set_property_helper(bus, obj, interface, prop_name, prop_value):
    log_msg = "Setting property {0} on from: {1} {2} {3} to {4}".format(prop_name, bus, obj, interface, prop_value)
    syslog(LOG_DEBUG, log_msg)
    dbus_iface = get_interface_helper(bus, obj, "org.freedesktop.DBus.Properties")
    return dbus_iface.Set(interface, prop_name, to_variant(prop_value))


# todo add support for remaining variant types
# todo add support for list and dict objects
def to_variant(value):
    result = value
    if isinstance(value, str):
        result = Variant('s', value)
    elif isinstance(value, bool):
        result = Variant('b', value)
    elif isinstance(value, bytearray):
        result = Variant('ay', value)
    return result
