#!/sbin/openrc-run

name="dnsmasq"
description="A lightweight DNS and DHCP server"
extra_commands="checkconfig"
extra_started_commands="reload"
description_checkconfig="Check configuration syntax"

command="/usr/sbin/dnsmasq"
supervisor=s6

default_config_file="/etc/bifrost/dnsmasq.default.conf"
config_file="/etc/dnsmasq.conf"
repeater_iface="${BIFROST_RUNTIME_REPEATER_IFACE:-BIFROST_CONFIG_REPEATER_IFACE}"

depend() {
  provide dns
  need localmount net s6-svscan bifrost-init
  after bootmisc bifrost-init
  use logger
}

start_pre() {
  # Replace the existing config file with a copy of the default config
  cp "${default_config_file}" "${config_file}"

  # Make sure the dynamic config doesn't append to the end of an existing line
  printf "\n\n" >> "${config_file}"

  echo "interface=${repeater_iface}" >> "${config_file}"

  checkconfig 2>&1 || return 1
  /lib/rc/bin/checkpath -m 0640 -o dnsmasq:dnsmasq -f "/data/dnsmasq.leases" || return 1
}

reload() {
  ebegin "Reloading $RC_SVCNAME"
  /bin/s6-svc -h ${s6_service_path}
  eend $?
}

checkconfig() {
  /lib/rc/bin/ebegin "Checking ${name} configuration"
  $command --test
  /lib/rc/bin/eend $?
}
