#!/usr/bin/env python

from syslog import syslog, LOG_DEBUG, LOG_ERR, LOG_CRIT
from time import sleep
from dbus_helper import get_interface_helper, get_property_helper

BUS_NAME = "org.freedesktop.systemd1"
OBJ_PATH = "/org/freedesktop/systemd1"
UNIT_IFACE_NAME = "org.freedesktop.systemd1.Unit"
MANAGER_IFACE_NAME = "org.freedesktop.systemd1.Manager"
MANAGER_INTERFACE = get_interface_helper(BUS_NAME, OBJ_PATH, MANAGER_IFACE_NAME)


def list_units():
    syslog("Listing systemd units")
    return MANAGER_INTERFACE.ListUnits()


def system_restart():
    syslog(LOG_CRIT, "Restarting system")
    return MANAGER_INTERFACE.Reboot()


def unit_exists(unit):
    syslog("Checking if {0} exists".format(unit))
    return filter(lambda x: x[0] == unit, list_units())


# Will raise exception if unit does not exist
def unit_get_unit_obj(unit):
    syslog(LOG_DEBUG, "Getting unit object for unit: {0}".format(unit))
    return MANAGER_INTERFACE.GetUnit(unit)


def unit_get_state(unit, state="Active"):
    unit_obj = unit_get_unit_obj(unit)
    syslog(LOG_DEBUG, "Getting state for unit: {0}".format(unit))
    return get_property_helper(BUS_NAME, unit_obj, UNIT_IFACE_NAME, "{0}State".format(state))


def unit_start(unit, mode="fail"):
    syslog(LOG_DEBUG, "Starting unit: {0} with mode: {1}".format(unit, mode))
    return MANAGER_INTERFACE.StartUnit(unit, mode)


def unit_stop(unit, mode="fail"):
    syslog(LOG_DEBUG, "Stopping unit: {0} with mode: {1}".format(unit, mode))
    return MANAGER_INTERFACE.StopUnit(unit, mode)


def unit_restart(unit, mode="fail"):
    syslog(LOG_DEBUG, "Restarting unit: {0} with mode: {1}".format(unit, mode))
    return MANAGER_INTERFACE.RestartUnit(unit, mode)


def unit_restart_sync(unit, mode="fail"):
    syslog(LOG_DEBUG, "Stopping unit: {0} with mode: {1}".format(unit, mode))
    MANAGER_INTERFACE.RestartUnit(unit, mode)
    unit_wait_until_state(unit, 'active')


def unit_wait_until_state(unit, target_state, timeout=60):
    sleep_unit = 1
    try:
        while True:
            current_state = unit_get_state(unit)
            syslog(LOG_DEBUG, "Unit: {0} has state: {1}".format(unit, current_state))
            if current_state == target_state:
                syslog(LOG_DEBUG, "Unit: {0} has reached state: {1}".format(unit, current_state))
                break
            else:
                sleep(sleep_unit)
            timeout -= sleep_unit
            if timeout < 0:
                raise RuntimeError("Timeout waiting for unit: {0} to reach state: {1}".format(unit, target_state))
    except Exception as ex:
        syslog(LOG_ERR, ex.message)
        raise ex
