#!/bin/bash
. /tmp/bifrost/exit_codes.sh

build_dir="${BUILD_TMP}"

enable_service() {
  local svc=${1}
  local s6svc="/var/svc.d/${svc}"

  echo "Enabling ${svc}"
  [ -f "/etc/init.d/${svc}" ] || exit ${initd_svc_doesnt_exist_error}
  /lib/rc/bin/checkpath -f -m 700 -o root:root "/etc/init.d/${svc}" > /dev/null
  rc-update add ${svc} default > /dev/null

  if grep -q "supervisor=s6" "/etc/init.d/${svc}"; then
    [ -d "${s6svc}" ] || exit ${s6_svc_dir_doesnt_exist_error}
    [ -f "${s6svc}/run" ] || exit ${s6_svc_run_doesnt_exist_error}
    /lib/rc/bin/checkpath -f -m 700 -o root:root "${s6svc}/run" > /dev/null
    if [ -f "${s6svc}/finish" ]; then
        /lib/rc/bin/checkpath -f -m 700 -o root:root "${s6svc}/finish" > /dev/null
    fi
  fi
}
###########################################
# Create users
###########################################
addgroup -S bifrost
addgroup -S hostapd
addgroup -S logger
addgroup -S logrotate

adduser -S -D -H -h /dev/null -G logger -g logger logger
adduser -S -D -H -h /dev/null -G logrotate -g logrotate logrotate

###########################################
# If bash is installed, use it instead of ash
###########################################
sed -i "s|/bin/ash|/bin/bash|" /etc/passwd

###########################################
# Enable services
###########################################
echo "Enabling services"
services=(s6 dbus syslog-ng crond openntpd rngd haveged dnsmasq hostapd bifrost-init)

for svc in "${services[@]}"; do
  enable_service ${svc}
done

###########################################
# Set defaults
###########################################
echo "Setting defaults"
${build_dir}/set-defaults.sh || exit $?

###########################################
# Set permissions and ownership
###########################################
echo "Setting permissions"
# init scripts
chmod 700 /etc/init.d/*

###########################################
# Validate config
###########################################
echo "Validating config"
services=(openntpd dnsmasq syslog-ng)

for svc in "${services[@]}"; do
  err="${svc//-/_}_config_error"
  source "/etc/init.d/${svc}"
  checkconfig || exit ${!err}
done

###########################################
# Cleanup
###########################################
echo "Cleaning up"
rm -rf ${build_dir}
