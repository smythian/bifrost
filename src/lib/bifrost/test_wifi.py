#!/usr/bin/env python

import unittest
import get_channel
from pkg_resources import resource_string
from mock import patch
from subprocess import CalledProcessError, check_output


class TestWifiCalculations(unittest.TestCase):
    def test_dbm_to_quality(self):
        self.assertEqual(get_channel.__dbm_to_quality__(-100), 0)
        self.assertEqual(get_channel.__dbm_to_quality__(-20), 100)
        self.assertEqual(get_channel.__dbm_to_quality__(-75), 50)
        self.assertEqual(get_channel.__dbm_to_quality__(-96), 8)

    def test_calculate_degraded_signal(self):
        self.assertEqual(get_channel.__calculate_degraded_signal__(100, 1), 50)
        self.assertEqual(get_channel.__calculate_degraded_signal__(100, 2), (100 / 3.0))
        self.assertEqual(get_channel.__calculate_degraded_signal__(100, 3), 25)

    def test_get_channels(self):
        self.assertEqual(get_channel.__get_channels__(), list(range(1, 12)))

    def test_parse_iw_scan(self):
        scan_one = resource_string(__name__, 'test_sample_iw_scan.txt')
        scan_two = resource_string(__name__, 'test_sample_iw_scan_2.txt')
        scan_three = resource_string(__name__, 'test_sample_iw_scan_3.txt')
        self.assertEqual(get_channel.__parse_iw_scan__(scan_one), 7)
        self.assertIsNone(get_channel.__parse_iw_scan__(""))
        self.assertIsNone(get_channel.__parse_iw_scan__(scan_two))
        self.assertIsNone(get_channel.__parse_iw_scan__(scan_three))

    @patch('%s.__scan__' % get_channel.__name__)
    def test_scan(self, mock_scan):
        mock_scan.return_value = resource_string(__name__, 'test_sample_iw_scan.txt')
        self.assertEqual(get_channel.scan('wlan0', 1), mock_scan.return_value)
        self.assertEqual(get_channel.scan('wlan0', 0), 3)
        mock_scan.side_effect = CalledProcessError(1, ["iw", "dev", "wlan0", "scan"], None)
        self.assertEqual(get_channel.scan('wlan0', 1), 3)


if __name__ == '__main__':
    unittest.main()
