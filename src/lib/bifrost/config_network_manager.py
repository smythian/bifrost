#!/usr/bin/env python

import argparse

from network_manager import set_device_property, get_device_property, activate_connection
from sys import exit
from syslog import syslog, LOG_ERR

UUID = '9686ac42-da80-4d18-9779-a06a151e6725'


def parse_args():
    parser = argparse.ArgumentParser(description='''
    Configure network manager to manage the interface used to connect to the 
    existing wireless LAN and to not manage the interface that will run the
    repeater access point. For to add_connection the specified wireless interface to the network with the specified SSID
    using the specified pre-shared key.
    ''')
    parser.add_argument('-r', '--repeater-iface')
    parser.add_argument('-s', '--wlan-ssid')
    parser.add_argument('-w', '--wlan-iface')
    parser.add_argument('-p', '--wlan-psk')
    return parser.parse_args()


def set_mgmt_state(iface, should_manage):
    set_device_property(iface, 'Managed', should_manage)
    if get_device_property(iface, 'Managed') != should_manage:
        syslog(LOG_ERR, "Unable to set management for {0} to {1}".format(iface, should_manage))
        exit(1)


def main():
    args = parse_args()
    w_iface = args.wlan_iface
    set_mgmt_state(w_iface, True)
    activate_connection(UUID, args.wlan_ssid, args.wlan_psk, w_iface)
    # Don't disable mgmt of the repeater iface until the wlan iface is configured.
    # Otherwise there is a potential to knock the device offline requiring manual intervention
    set_mgmt_state(args.repeater_iface, False)


if __name__ == '__main__':
    main()
