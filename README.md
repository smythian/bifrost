# Bifrost

[![License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://gitlab.com/smythian/bifrost/blob/master/LICENSE)
[![Maintenance](https://img.shields.io/badge/maintained-Yes-brightgreen.svg)](https://gitlab.com/smythian/bifrost/activity)

A balenaOS-based WiFi repeater designed to run on a Raspberry Pi 3.

## Getting Started
Why a cloud-connected Pi-based WiFi repeater? Because I could, but that doesn't mean that you should. This was my 2019 Pi Day project, but I doubt that I'll continue using it. Max throughput that I've seen is ~6Mbps.

### Hardware prerequisites
* [Raspberry Pi 3](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/)
* USB 802.11 b/g/n WiFi adapter (i.e. 2.4GHz). I'm using a TP-Link TLWN722N, but the [TP-Link TL-WN725N](https://www.tp-link.com/us/products/details/cat-5520_TL-WN725N.html) also works. 

### Balena configuration 
* Create a [BalenaCloud](https://www.balena.io/cloud/) account
* [Add your SSH key to your Balena account](https://dashboard.balena-cloud.com/preferences/sshkeys)
* [Create a Bifrost application](https://dashboard.balena-cloud.com/apps/). The `Starter` type is fine.
* Download balenaOS from the Balena application dashboard. I recommend the `Production` edition (unless you're doing local development, see below), and setting the network connection to `Ethernet Only`.
* Flash balenaOS to a micro SD card. I recommend using Balena's [Etcher tool](https://www.balena.io/etcher/)
* Add the [environment variables](https://dashboard.balena-cloud.com/devices/<device_uuid>/envvars) to your Balena Cloud app
  * `BIFROST_RUNTIME_REPEATER_PSK` The secret key used to connect devices to the network
  * `BIFROST_RUNTIME_REPEATER_SSID` (Optional) The name of the extended wireless network. Defaults to `Bifrost`
  * `BIFROST_RUNTIME_REPEATER_IFACE` (Optional) The interface to use for the access point. Defaults to `wlan0` (the RPi3 built-in WiFi interface)
  * `BIFROST_RUNTIME_WLAN_PSK` The secret key used to connect Bifrost to the network that you want to extend
  * `BIFROST_RUNTIME_WLAN_SSID` The name of the wireless network to serve from the extender
  * `BIFROST_RUNTIME_WLAN_IFACE` (Optional) The interface to use for the access point. Defaults to `wlan1` 

### Local development prerequisites
* Balena CLI - To push devices running development builds on your local network
* [NVM](https://github.com/creationix/nvm) [**Optional**] 
* [Node and NPM](https://nodejs.org/en/download/) - Needed to use the Balena CLI
* [Balena CLI (for local development)](https://www.balena.io/docs/reference/cli)
* Python [**Optional**] Needed to test the code in `./src/lib/bifrost`
  * [Python 2.7](https://www.python.org/download/releases/2.7/)
  * [pip](https://pip.pypa.io/en/stable/installing/)
  * [pipenv](https://pypi.org/project/pipenv/)
* Log into the Balena CLI by using `balena login`.

# Deploying
Add the Balena Cloud git: `git remote add balena <username>@git.balena-cloud.com:<username>/<application_name>.git`. Then push to the `master` branch on the `balena` remote.

## Versioning

I use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/smythian/bifrost/tags).

## Authors

* [Eric Smyth](https://gitlab.com/smythian)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE.md) file for details. 
