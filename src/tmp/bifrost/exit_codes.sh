#!/bin/bash

# enable-services.sh
initd_svc_doesnt_exist_error=1
s6_svc_dir_doesnt_exist_error=2
s6_svc_run_doesnt_exist_error=3

# set-defaults.sh
missing_config_value_error=4

# validate-config
openntpd_config_error=5
hostapd_config_error==6
dnsmasq_config_error=7
syslog_ng_config_error=8
